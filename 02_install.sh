#!/bin/bash
HERE=$(pwd)
source ../../env.sh

if [ "${TRAINING_ID_SHORT}" = "yocto" ]; then
INSTALL_PATH=${PWD##*/}
  set -x
  if [ -d "${HOST_NFS_EXPORTED_EXT_SDK_ROOTFS_ROOT}/home/${USER}/${INSTALL_PATH}" ]; then
    sudo rm -rf ${HOST_NFS_EXPORTED_EXT_SDK_ROOTFS_ROOT}/home/${USER}/${INSTALL_PATH}
  fi

  sudo mkdir -p ${HOST_NFS_EXPORTED_EXT_SDK_ROOTFS_ROOT}/home/${USER}/${INSTALL_PATH}
  sudo chown ${USER}:${USER} ${HOST_NFS_EXPORTED_EXT_SDK_ROOTFS_ROOT}/home/${USER}/${INSTALL_PATH}

  cp -r * ${HOST_NFS_EXPORTED_EXT_SDK_ROOTFS_ROOT}/home/${USER}/${INSTALL_PATH}
  rm -rf ${HOST_NFS_EXPORTED_EXT_SDK_ROOTFS_ROOT}/home/${USER}/${INSTALL_PATH}/raw-for-slides
  rm -rf ${HOST_NFS_EXPORTED_EXT_SDK_ROOTFS_ROOT}/home/${USER}/${INSTALL_PATH}/for-slides
  set +x
else # eglisa ?
  set -x
  sudo mkdir -p ${NFS_EXPORTED_ROOTFS_ROOT}$(pwd)
  sudo chown -R ${USER}:${USER} ${NFS_EXPORTED_ROOTFS_ROOT}$(pwd)
  cp -r * ${NFS_EXPORTED_ROOTFS_ROOT}$(pwd)
  rm -rf ${NFS_EXPORTED_ROOTFS_ROOT}$(pwd)/raw-for-slides
  rm -rf ${NFS_EXPORTED_ROOTFS_ROOT}$(pwd)/for-slides
  set +x
fi

cd ${HERE}

